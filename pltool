#!/usr/bin/env python2

import re
import os
import sys
import stat
import subprocess

##########################################

def fatal(err, *fmt_args, **fmt_kwargs):
	print err.format(*fmt_args, **fmt_kwargs)
	sys.exit(1)

def hr_size(size_bytes):
	n = float(size_bytes)
	m = ['GB', 'MB', 'KB', 'B']

	while n > 1000.0 and len(m) > 1:
		n = n / 1000.0
		m.pop()

	return '{0:.2f} {1}'.format(n, m[-1])

def first_exist_parent(path):
	ppath = path
	while True:
		try:
			stat = os.stat(ppath)
			return ppath
		except OSError:
			ppath, _ = os.path.split(ppath)
			continue

def fs_freespace(path):
	stat = os.statvfs(path)
	return stat.f_frsize * stat.f_bavail

def fs_files_equal(file_a, file_b):
	try:
		if os.stat(file_a).st_size == os.stat(file_b).st_size:
			return True
	except: pass
	return False

def duplicate_name_mod(name, idx):
	return '({1}) {0}'.format(name, idx)

CLEAN_REGEXP = re.compile('^[0-9]+[ _]*([\.\-]+[ _]*)?')

def clean_file_name(name):
	name_fix, ext = os.path.splitext(name)
	name_fix = name_fix.strip()
	name_fix = CLEAN_REGEXP.sub('', name_fix)
	return (name_fix + ext) if len(name_fix) != 0 else name

##########################################

class PlayList:
	def __init__(self, path):
		self.path = path
		self.files = []
		self.file_names = {}
		self.file_names_set = set()
		self.file_sizes = {}
		self.data_size = 0
		self.hr_data_size = '<unknown>'

	def parse(self):
		with open(self.path, 'r') as f: self._parse(f)
		return self

	def add_file(self, file_path):
		if file_path in self.file_names:
			return

		self.files.append(file_path)

		dir_path, file_name = os.path.split(file_path)
		file_name = clean_file_name(file_name)

		if file_name in self.file_names_set:
			n = 1
			while duplicate_name_mod(file_name, n) in self.file_names_set:
				n = n + 1
			file_name = duplicate_name_mod(file_name, n)

		self.file_names[file_path] = file_name
		self.file_names_set.add(file_name)

	def verify(self):
		for filename in self.files:
			file_stats = os.stat(filename)
			if stat.S_ISREG(file_stats.st_mode):
				file_size = file_stats.st_size
				self.file_sizes[filename] = file_size
				self.data_size = self.data_size + file_size
				# TODO:: Handle symlinks
			else:
				print 'Not a file: {}'.format(filename)
				self.files.remove(filename)
		self.hr_data_size = hr_size(self.data_size)
		return self

class M3UPlaylist(PlayList):
	def __init__(self, path):
		PlayList.__init__(self, path)

	def _parse(self, file):
		for line in file:
			if line[0] == '#':
				continue
			full_name = line.translate(None,"\n")
			self.add_file(full_name)

PLAYLIST_PARSERS = {
	'.m3u': M3UPlaylist
}

def playlist(filename):
	name, extension = os.path.splitext(filename)
	try:
		pl_parser = PLAYLIST_PARSERS[extension.lower()]
	except:
		fatal('Unknown playlist type: {}', extension)
	return pl_parser(filename)

##########################################

class NullDestination:
	def __init__(self):
		self.path = '<null>'
		self.freespace = 0
		self.hr_freespace = '?'
		self.exist = None

	def prepare(self):
		fatal('Cannot copy to nowhere.')

	def accept(self, source_file, name):
		fatal('Cannot copy to nowhere.')

class DirectoryDestination:
	def __init__(self, path):
		path = os.path.abspath(path)
		self.path = path
		par = first_exist_parent(path)
		self.exist = (par == path)
		self.freespace = fs_freespace(par)
		self.hr_freespace = hr_size(self.freespace)

	def prepare(self):
		if not self.exist:
			os.makedirs(self.path)

	def accept(self, source_file, name):
		dst_path = os.path.join(self.path, name)
		if fs_files_equal(dst_path, source_file): return
		ret = subprocess.call(['cp', source_file, dst_path])
		if ret != 0:
			fatal('Copying of {0}\n\tto {1}\n\tfailed with code {2}', source_file, dst_path, ret)

class AdbDirectoryDestination:
	def __init__(self, path):
		self.path = path
		self.exist = None
		self.freespace = -1
		self.hr_freespace = '?'

	def prepare(self):
		ret = subprocess.call(['adb', 'shell', 'mkdir', '-p', self.path])

		if ret != 0:
			fatal('Failed to initialize ADB directory.')

	def accept(self, source_file, name):
		dst_path = os.path.join(self.path, name)

		ret = subprocess.call(['adb', 'push', source_file, dst_path])

		if 0 != ret:
			fatal('Could not send {0}\n\tto {1}', name, dst_path)

def destination(path):
	if path.startswith('adb::'):
		return AdbDirectoryDestination(path[5:])
	return DirectoryDestination(path)

##########################################

def copy_playlist():
	pl = args.input_file.parse().verify()
	od = args.output_directory

	od.prepare()

	bytes_copied = 0

	for source_path in pl.files:
		bytes_copied = bytes_copied + pl.file_sizes[source_path]
		print '[{0:3d}%, {1} / {2}] {3}'.format(
				(100 * bytes_copied) / pl.data_size,
				hr_size(bytes_copied), pl.hr_data_size,
				pl.file_names[source_path]
			)
		od.accept(source_path, pl.file_names[source_path])

def stat_playlist():
	pl = args.input_file.parse().verify()
	print 'Source:'
	print '{}:'.format(pl.path)
	print '{} files.'.format(len(pl.files))
	print 'Total size: {}'.format(pl.hr_data_size)
	print ''
	od = args.output_directory
	print 'Destination:'
	print '{}:'.format(od.path)
	print 'Created: {}'.format('Yes' if od.exist else ('No' if od.exist is not None else '?'))
	print 'Free: {}'.format(od.hr_freespace)

##########################################

HELP_STRING = """
Playlist tool - copy playlist contents.
"""

HELP_EPILOG = """
EXAMPLES:

Copy to local filesystem:

$	pltool Music.m3u /media/flashcard/Music

Copy to Android device using ADB:

$	pltool Music.m3u adb::/sdcard/Music
"""

##########################################

import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=HELP_STRING, epilog=HELP_EPILOG, prog='pltool')

action_group = parser.add_mutually_exclusive_group()
action_group.add_argument(
	'--copy', dest='action', action='store_const', const=copy_playlist, default=copy_playlist,
	help='copy the playlist (default)')

action_group.add_argument(
	'--stat', dest='action', action='store_const', const=stat_playlist,
	help='collect playlist statistics instead of copying')

parser.add_argument(
	'input_file', metavar='IF', type=playlist,
	help='name of input (playlist) file')

parser.add_argument(
	'output_directory', metavar='OD', type=destination, nargs='?', default=NullDestination(),
	help='path to output directory')

args = parser.parse_args()

args.action()
